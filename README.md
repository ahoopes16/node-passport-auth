# README

This repository is an example of how to implement an authentication API using [Passport.js](http://www.passportjs.org/), which should allow us to implement social logins as well as logins with our own usernames/passwords.

## How to Run

1. Clone this repo locally and navigate to it in a terminal window
1. Run the command `npm install` or `yarn`, depending on your preference
1. Create a file in the root directory called `.env` and copy the contents of `.env.example` into it. Fill out the fields with your own information. I have provided defaults where possible.
1. Run the command `npm run start` or `yarn start`
1. Import the included Postman collection and environment into Postman

Behold! You can run the API and register a user, authenticate with them, etc. Play around with the tokens and see if you can break them somehow. Please let me know if you can.

## Author

-   [Alex Hoopes](mailto:alexh@haystackdev.com)

## Resources

-   [Digital Ocean - API Authentication with JWT and Passport](https://www.digitalocean.com/community/tutorials/api-authentication-with-json-web-tokensjwt-and-passport)
-   [jwt.io](https://jwt.io)
-   [Passport.js](http://www.passportjs.org/)
