const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');

let { refreshTokens } = require('../database');

const router = express.Router();

router.post(
    '/register',
    passport.authenticate('register', { session: false }),
    (req, res) => {
        res.json({
            message: 'User registered successfully!',
            user: req.user,
        });
    }
);

router.post('/login', async (req, res, next) => {
    passport.authenticate('login', async (err, user, info) => {
        try {
            if (err || !user) {
                const error = new Error('An error occurred');
                return next(error);
            }

            req.login(user, { session: false }, async error => {
                if (error) {
                    return next(error);
                }

                const body = { id: user.id, email: user.email };

                const accessToken = jwt.sign(
                    { user: body },
                    process.env.ACCESS_TOKEN_SECRET,
                    { expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN }
                );
                const refreshToken = jwt.sign(
                    { user: body },
                    process.env.REFRESH_TOKEN_SECRET
                );

                refreshTokens.push(refreshToken);

                return res.json({ accessToken, refreshToken });
            });
        } catch (error) {
            return next(error);
        }
    })(req, res, next);
});

router.get('/facebook/callback', (req, res, next) => {
    passport.authenticate('facebook', { session: false }, (err, user, info) => {
        req.login(user, { session: false }, err => {
            if (err) {
                res.status(400).send({ err });
            }

            const accessToken = jwt.sign(
                { user },
                process.env.ACCESS_TOKEN_SECRET,
                { expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN }
            );
            const refreshToken = jwt.sign(
                { user },
                process.env.REFRESH_TOKEN_SECRET
            );

            refreshTokens.push(refreshToken);

            return res.json({ accessToken, refreshToken, user, info });
        });
    })(req, res, next);
});

router.post('/token', async (req, res, next) => {
    const { refreshToken } = req.body;

    if (!refreshToken) {
        return res.sendStatus(401);
    }

    if (!refreshTokens.includes(refreshToken)) {
        return res.sendStatus(403);
    }

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        }

        const body = { id: user.id, email: user.email };
        const accessToken = jwt.sign(
            { user: body },
            process.env.ACCESS_TOKEN_SECRET,
            {
                expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN,
            }
        );

        res.json({ accessToken });
    });
});

router.post('/logout', async (req, res, next) => {
    const { refreshToken } = req.body;

    refreshTokens = refreshTokens.filter(token => token !== refreshToken);

    res.json({ message: 'Logout successful!' });
});

module.exports = router;
