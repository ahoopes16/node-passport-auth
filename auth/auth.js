const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const FacebookStrategy = require('passport-facebook').Strategy;
const uniqid = require('uniqid');

const { hashPassword, isValidPassword } = require('./helpers');
let { users } = require('../database');

passport.use(
    'register',
    new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
        },
        async (email, password, done) => {
            try {
                const existingUser = users.find(u => u.email === email);
                if (existingUser) {
                    throw new Error(
                        `A user with the email "${email}" already exists`
                    );
                }

                const user = { id: uniqid(), email };
                const hashedPassword = await hashPassword(password);
                users.push({ ...user, password: hashedPassword });

                return done(null, user);
            } catch (error) {
                done(error);
            }
        }
    )
);

passport.use(
    'login',
    new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
        },
        async (email, password, done) => {
            try {
                const user = users.find(u => u.email === email);

                if (!user) {
                    return done(null, false, { message: 'User not found!' });
                }

                const validPassword = await isValidPassword(user, password);
                if (!validPassword) {
                    return done(null, false, {
                        message: 'Incorrect password!',
                    });
                }

                return done(null, user, { message: 'Logged in successfully!' });
            } catch (error) {
                done(error);
            }
        }
    )
);

passport.use(
    new JWTStrategy(
        {
            secretOrKey: process.env.ACCESS_TOKEN_SECRET,
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        },
        async (token, done) => {
            try {
                return done(null, token.user);
            } catch (error) {
                done(error);
            }
        }
    )
);

passport.use(
    new FacebookStrategy(
        {
            clientID: process.env.FACEBOOK_APP_ID,
            clientSecret: process.env.FACEBOOK_APP_SECRET,
            callbackURL: 'http://localhost:3000/auth/facebook/callback',
        },
        (accessToken, refreshToken, profile, done) => {
            console.log('profile', profile);

            const existingUser = users.find(u => u.id === profile.id);
            if (!existingUser) {
                const user = {
                    id: profile.id,
                    email: profile.email,
                    provider: 'Facebook',
                };
                users.push(user);
            }

            return done(null, user);
        }
    )
);
