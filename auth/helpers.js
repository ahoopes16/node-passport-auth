const bcrypt = require('bcrypt');

function hashPassword(password) {
    return bcrypt.hash(password, 10);
}

function isValidPassword(user, password) {
    return bcrypt.compare(password, user.password);
}

module.exports = {
    hashPassword,
    isValidPassword,
};
