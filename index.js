require('dotenv').config();
const express = require('express');
const passport = require('passport');
const bodyParser = require('body-parser');

require('./auth/auth');

const routes = require('./routes/routes');
const secureRoutes = require('./routes/secure-routes');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use('/auth', routes);
app.use(
    '/auth/facebook',
    passport.authenticate('facebook', { scope: ['email'] })
);

app.use(
    '/user',
    passport.authenticate('jwt', { session: false }),
    secureRoutes
);

app.use((err, req, res) => {
    res.status(err.status || 500);
    res.json({ error: err });
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}!`);
});
